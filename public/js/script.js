// ------------ Placements aléatoires des hexagones ------------
function getRandomInt(max) {
    return Math.floor(Math.random() * max);
}

function randomPlace() { }

function randomPattern() {
    document.querySelectorAll(".pattern").forEach((element) => {
        element.style.left = `${getRandomInt(150)}vw`;
        element.style.top = `${getRandomInt(250)}vh`;
    });
}

function randomPattern2() {
    document.querySelectorAll(".pattern2").forEach((element) => {
        element.style.left = `${getRandomInt(150)}vw`;
        element.style.top = `${getRandomInt(250)}vh`;
    });
}

function randomPattern3() {
    document.querySelectorAll(".pattern3").forEach((element) => {
        element.style.left = `${getRandomInt(150)}vw`;
        element.style.top = `${getRandomInt(250)}vh`;
    });
}

window.addEventListener("load", randomPattern);
window.addEventListener("load", randomPattern2);
window.addEventListener("load", randomPattern3);


// ------------ Apparition/Disparition des colonnes ------------

var colonne1 = document.querySelectorAll(".colonne1");
var colonne2 = document.querySelectorAll(".colonne2");
var colonne3 = document.querySelectorAll(".colonne3");

var colonne1DisplayState = true;
var colonne2DisplayState = false;
var colonne3DisplayState = false;

var bouton1 = document.querySelectorAll(".sup01");
var bouton2 = document.querySelectorAll(".sup02");

for (var i = 0; i < bouton1.length; i++) {
    var bt = bouton1[i];
    bt.addEventListener("click", function () {
        if (colonne2DisplayState === false) {
            colonne2DisplayState = true;
        }
        else {
            colonne2DisplayState = false;
            colonne3DisplayState = false;
        }
        debug();
        applyState();
        randomPattern();
        randomPattern2();
        randomPattern3();
    });
}

for (var i = 0; i < bouton1.length; i++) {
    var bt = bouton2[i];
    bt.addEventListener("click", function () {
        if (colonne3DisplayState === false) {
            colonne3DisplayState = true;
        }
        else {
            colonne3DisplayState = false;
        }
        debug();
        applyState();
        randomPattern();
        randomPattern2();
        randomPattern3();
    });
}

function applyState() {
    var main = document.getElementById("body");


    if (colonne2DisplayState === true && colonne3DisplayState === false) {

        main.style.width = 150 + "vw";
        main.style.overflowX = "scroll";

        colonne2.forEach(element => {
            element.style.display = "block";
        });
        colonne3.forEach(element => {
            element.style.display = "none";
        });
    }

    if (colonne2DisplayState === true && colonne3DisplayState === true) {

        main.style.width = 150 + "vw";
        main.style.overflowX = "scroll";

        colonne2.forEach(element => {
            element.style.display = "block";
        });
        colonne3.forEach(element => {
            element.style.display = "block";
        });
    }

    if (colonne2DisplayState === false && colonne3DisplayState === false) {

        main.style.width = 100 + "vw";
        main.style.overflowX = "hidden";

        colonne2.forEach(element => {
            element.style.display = "none";
        });
        colonne3.forEach(element => {
            element.style.display = "none";
        });
    }
}

function debug() {
    console.log("colonne2DisplayState", colonne2DisplayState, "colonne3DisplayState", colonne3DisplayState);
}


// ------------ AlertBox ------------
var check1 = document.getElementById("check1");
var check2 = document.getElementById("check2");

var alertBox = document.getElementById("alertBox");
var alertBoxDisplayState = false;

var closeAlert = document.getElementById("close");

check1.addEventListener("change", function () {
    if (check1.checked && check2.checked) {
        alertBoxDisplayState = true;
        alertBox.classList.remove("alertBox-off");
        alertBox.classList.add("alertBox-on");
    }
});

check2.addEventListener("change", function () {
    if (check1.checked && check2.checked) {
        alertBoxDisplayState = true;
        alertBox.classList.remove("alertBox-off");
        alertBox.classList.add("alertBox-on");
    }
});

closeAlert.addEventListener("click", function () {
    alertBox.classList.remove("alertBox-on");
    alertBox.classList.add("alertBox-off");
    check1.checked = false;
    check2.checked = false;
});